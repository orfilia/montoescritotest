package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {
    private static Map<Integer, String> parseoNumeros = new HashMap<Integer, String>();

    public static void main(String[] args) {
        System.out.println(monto(1027410));
    }

    public static String monto (int x){
        String Numero = "";
        if (x >= 0 && x <= 15){
            Numero = getMontoEscrito(x);
        }
        if (x > 15 && x < 20){
            Numero =  ("dieci"+getMontoEscrito(unidades(x)));
        }
        if (x > 20 && x < 30){
            Numero =  ("veinti"+getMontoEscrito(unidades(x)));
        }
        if (x > 30 && x < 40){
            Numero =  ("treinta y "+getMontoEscrito(unidades(x)));
        }
        if (x > 40 && x < 50){
            Numero =  ("cuarenta y "+getMontoEscrito(unidades(x)));
        }
        if (x > 50 && x < 60){
            Numero =  ("cincuenta y "+getMontoEscrito(unidades(x)));
        }
        if (x > 60 && x < 70){
            Numero =  ("sesenta y "+getMontoEscrito(unidades(x)));
        }
        if (x > 70 && x < 80){
            Numero =  ("setenta y "+getMontoEscrito(unidades(x)));
        }
        if (x > 80 && x < 90){
            Numero =  ("ochenta y "+getMontoEscrito(unidades(x)));
        }
        if (x > 90 && x < 100){
            Numero =  ("noventa y "+getMontoEscrito(unidades(x)));
        }
        if (x == 20 || x == 30 || x == 40 || x == 50 || x == 60 || x == 70 || x == 80 || x == 90) {
            Numero = (getMontoEscrito(x));
        }
        if (x > 100 && x < 200){
            Numero =  ("ciento ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 200 && x < 300){
            Numero = ("doscientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 300 && x < 400){
            Numero = ("trescientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 400 && x < 500){
            Numero = ("cuatrocientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 500 && x < 600){
            Numero =  ("quinientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 600 && x < 700){
            Numero = ("seiscientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 700 && x < 800){
            Numero = ("setecientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 800 && x < 900){
            Numero = ("ochocientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x > 900 && x < 1000){
            Numero = ("novecientos ");
            Numero = Numero + monto(ultimasDosCifras(x));
        }
        if (x == 100 || x == 200 || x == 300 || x == 400 || x == 500 || x == 600 || x == 700 || x == 800 || x == 900 || x == 1000 || x == 1000000){
            Numero = (getMontoEscrito(x));
        }
        if (x > 1000 && x < 1000000){
            if (tresPrimerasCifras(x) != 1) {
                Numero = monto(tresPrimerasCifras(x));
            }
            Numero = Numero + " mil ";
            if (ultimasTresCifras(x) != 0){
                Numero = Numero + monto(ultimasTresCifras(x));
            }
        }
        if (x > 1000000 && x <= 100000000){
            if (x > 1000000 && x < 2000000){
                Numero = "un millon ";
            }
            else{
                Numero = Numero + monto(cifrasMillones(x));
                Numero = Numero + " millones ";
            }
            if (ultimasSeisCifras(x) != 0){
                Numero = Numero + monto(ultimasSeisCifras(x));
            }
        }
        return Numero;
    }

    public static String getMontoEscrito(Integer valor) {

        String resultado = "";
        crearParseNumeros();
        if (parseoNumeros.containsKey(valor)) {
            resultado = parseoNumeros.get(valor);
        }
        return resultado;
    }

    private static void crearParseNumeros() {
        parseoNumeros.put(0, "cero");
        parseoNumeros.put(1, "uno");
        parseoNumeros.put(2, "dos");
        parseoNumeros.put(3, "tres");
        parseoNumeros.put(4, "cuatro");
        parseoNumeros.put(5, "cinco");
        parseoNumeros.put(6, "seis");
        parseoNumeros.put(7, "siete");
        parseoNumeros.put(8, "ocho");
        parseoNumeros.put(9, "nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(200, "docientos");
        parseoNumeros.put(300, "trescientos");
        parseoNumeros.put(400, "cuatrocientos");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(600, "seiscientos");
        parseoNumeros.put(700, "setecientos");
        parseoNumeros.put(800, "ochocientos");
        parseoNumeros.put(900, "novecientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
    }

    public static int unidades(int x) {
        int u = x % 10;
        return u;
    }

    public static int ultimasDosCifras (int x){
        int d = x % 100;
        return  d;
    }

    public static int ultimasTresCifras (int x){
        int d = x % 1000;
        return  d;
    }

    public static int tresPrimerasCifras (int x){
        int d = x / 1000;
        return  d;
    }

    public static int cifrasMillones (int x){
        int d = x / 1000000;
        return d;
    }

    public static int ultimasSeisCifras (int x){
        int d = x % 1000000;
        return d;
    }
}